# Convertir tiempo en horas, minutos y segundos en segundos

program time
   implicit none
   integer :: hrs, minu, seg, temp
      write (*,*) "Escribe la hora en hrs, minu, seg:"
      read (*,*) hrs, minu, seg
         temp = (3600)*( hrs ) + (60)*( minu ) + seg
      write (*,*) "La hora en segundos es:", temp
end program time