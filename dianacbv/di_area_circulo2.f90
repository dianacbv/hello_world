! Area de un cìrculo de radio r y declaracion de pi
program circulo2
  REAL, PARAMETER :: pi = 3.1416
  REAL :: r, area
     WRITE (*,*) "Escribe el radio r:"
     READ (*,*) r
       area = pi*r**2
     WRITE (*,*) "Area = ", area
end program circulo2