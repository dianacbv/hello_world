module deps
    contains

    SUBROUTINE read_file( Inten, dos_theta, theta )
        real, DIMENSION(301), intent(in out) :: Inten
        real, DIMENSION(3450), intent(in out) :: theta
        REAL, DIMENSION(301), INTENT(out) :: dos_theta
        integer :: i, j
        REAL :: x

        write(*,*) "inside SUBROUTINE 01"


        OPEN(UNIT=4,FILE='bi_pow_pat.dat',STATUS='OLD',ACTION='READ')
        OPEN(UNIT=3,FILE='element.dat',STATUS='OLD',ACTION='READ')
        write(*,*) "inside SUBROUTINE 02"


        Do i = 1,301
            READ(3,*) dos_theta(i), Inten(i)
            WRITE(*,*) "dos_theta, Inten", dos_theta(i), Inten(i)
        End Do


        Do j=1, 3450
            READ(4,*) theta(j)
            write(*,*) "in loop, j=", j

                !WRITE(*,*) theta(j)
                !IF  (theta(j) == dos_theta(i)) THEN
                !    READ(3,*) dos_theta, Inten
                !    WRITE(*,*) theta, Inten
                !End If

        End Do

        CLOSE(UNIT=4)
        CLOSE(UNIT=3)
    RETURN
    END SUBROUTINE read_file

end module deps

!This program calculates the intensity for each pixel in the
!bidimensional espectra.
PROGRAM bi_pow_pat
    use deps
    IMPLICIT NONE

    REAL, DIMENSION(3450) :: x, y, xp, yp
    REAL, DIMENSION(3450) :: C, theta, Inten, dos_theta
    INTEGER :: xc, yc, i, j

    xc=1725
    yc=1725

    OPEN(UNIT=2,FILE='bi_pow_pat.dat',STATUS='REPLACE',ACTION='WRITE')
    !WRITE(2,*) "          x          y       x(i)              y(i)            C(i)            theta(i)"

    Do i=0,3450
        x(i)= i-xc
        y(i)=(3450-i)-xc
        C(i)=SQRT((x(i)**2) + (y(i)**2))
        theta(i)=atan((C(i))/862.5)*57.2957795
        WRITE(2,*) i, (3450-i), x(i), y(i), C(i), theta(i)
    End Do

    CLOSE(UNIT=2)

    CALL read_file( Inten, dos_theta, theta )

    !DO i=0, 3450
    !  WRITE(2,*) i, (3450-i), x(i), y(i), C(i), theta(i), Inten(i)
    !END DO

End Program bi_pow_pat

