PROGRAM Bidimensional
IMPLICIT NONE

INTEGER :: i, j, l
REAL :: C, local_dos_theta, Ibid1, Ibid2
REAL, DIMENSION(1501) :: dos_theta, Iuni
REAL, DIMENSION(345,345) :: Inten

!OPEN(UNIT=2,FILE='bi_pow_pat_2.dat',STATUS='REPLACE',ACTION='WRITE')
OPEN(UNIT=3,FILE='element.dat',STATUS='old',ACTION='read')

!Calculating the theta angle for each pixel to the sample position

    DO i=1,1501
        READ(3,*) dos_theta(i), Iuni(i)
    END DO

    DO i=1,345
        DO j=1,345
            C=SQRT(((i-172.5)**2)+(((345-j)-172.5)**2))
            local_dos_theta=(atan(C/86.25))*57.2957795
                DO l=1,1501
                    IF (local_dos_theta<=dos_theta(l+1) .and. local_dos_theta>=dos_theta(l)) THEN
                        Ibid1=Iuni(l)*(sin(dos_theta(l)))*57.2957795
                        Ibid2=Iuni(l+1)*(sin(dos_theta(l+1)))*57.2957795
                        Inten(i,j)=(Ibid1+Ibid2)/2
                    END IF
                END DO

        END DO
    END DO

DO i=1, 345

    WRITE(*,*) (Inten(i,j), j=1,345)

END DO

!CLOSE(UNIT=2)
CLOSE(UNIT=3)

END PROGRAM
