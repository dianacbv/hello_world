!Resolucion de ecuaciones cuadraticas 2
PROGRAM cuadratica
    REAL :: x1, x2, a, b, c, RAIZ1, RAIZ2, IMAG1, IMAG2
    WRITE (*,*) "Cuales son los valores de a, b y c?"
    READ (*,*) a, b, c
    D = b**2 - 4*a*c
    IF (D>0.0) THEN
        x1 = (-b + D**1/2)/(2*a)
        x2 = (-b - D**1/2)/(2*a)
        WRITE (*,*) "Las soluciones son x1:", x1, "x2:", x2
    ELSE IF (D == 0) THEN
        x1 = -(b/2*a)
        x2 = -(b/2*a)
        WRITE (*,*) "Las soluciones son x1:", x1, "x2:", x2
    ELSE IF (D<0.0) THEN
        RAIZ1 = ((-b)/(2*(a)))
        IMAG1 = (((-b)**1/2)/(2*(a)))
        RAIZ2 = (-(b)/(2*(a)))
        IMAG2 = -((((-b))**1/2)/(2*(a)))
        WRITE (*,*) "Las soluciones son x1:", COMPLEX(RAIZ1, IMAG1), "x2:", COMPLEX(RAIZ2, IMAG2)
    END IF
END PROGRAM cuadratica
