! Hola Luiso, tengo un problema, tanto tu programa modificado como
! el mio arrojan datos raros de x(i) y de y(i). He borrado y hecho
! de nuevo el archivo .dat pensando en que este dañado, pero el
! resultado es el mismo. Tambièn tuve que ponerle al do de READ que
! cerrara el loop con un while(i < 22). En fin, no se si tal vez sea
! un rollo de mi compilador maybe?. En fin èste es mi programa.
PROGRAM Leer_y_escribir
IMPLICIT NONE
    INTEGER :: i
    REAL, DIMENSION(21) :: x, y
    REAL :: x_tot=0, y_tot=0

    OPEN (unit = 2, file = "di_poli4.dat", status='old', action='read')

    !DO while(i < 22)
    DO i=1,21                       ! A mi me funcina bien con el ciclo asi
       READ (2, FMT=*) x(i), y(i)
    END DO
    CLOSE (unit=2)                  ! si terminamos con la lectura del fichero
                                    ! mejor lo serramos aqui mismo,
                                    ! no entiendo por que, pero si lo hacemos asi
                                    ! el compilador tiende a cuatrapearce menos

    DO i=1,21
        WRITE (*,*) x(i), y(i)
    END DO

    Do i=1,21
       x_tot=x_tot+x(i)
       y_tot=y_tot+y(i)
    END DO

    WRITE (*,*)"La sumatoria de las x(i) es:", x_tot
    WRITE (*,*)"La sumatoria de las y(i) es:", y_tot

END PROGRAM Leer_y_escribir
! si lo compilas con el comando "gfortran -Wall di_poli_leer_y_mostrar_02.f90"
! (sin commillas) te advierte sobre detalles como el uso de una variable
! sin inicializar



! Estos son los datos x(i) y y(i) que arroja el programa
!x(i)           y(i)
!   0.0000000       0.0000000
!   0.0000000      1.57784526E-39
!   0.0000000     -1.45752929E-05
!   0.0000000       0.0000000
!   0.0000000       0.0000000
!   0.0000000       0.0000000
!  3.98760561E-34   0.0000000
!  3.99104209E-34             NaN
!   0.0000000       0.0000000
! -9.86263270E-34   0.0000000
!  -1.4942064       0.0000000
!  1.57784526E-39   0.0000000
!  1.40129846E-45  1.03142573E-38
!  3.02481484E-39  1.03175364E-38
!   0.0000000      1.40129846E-45
!  5.46419521E-39   0.0000000
!   0.0000000      1.02061542E-38
!  1.65893279E-39  1.03181530E-38
!  -1.4942064     -1.45040103E-05
!  1.02420568E-38  1.40129846E-45
!  2.61486312E-33  1.40129846E-45
! x(tot) =  -2.9884129
! y(tot) =             NaN

