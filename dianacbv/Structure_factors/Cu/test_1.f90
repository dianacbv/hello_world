!test for module_1
PROGRAM TEST_1

USE tools_f
CHARACTER(LEN=20) :: sim1, cubic, element
INTEGER :: sim2
INTEGER, DIMENSION(7,3) :: hkl
REAL :: a, b, c, lambda, a1, a2, a3, a4, b1, b2, b3, b4, c1

OPEN(4,FILE='cell_data2.dat',STATUS='OLD',ACTION='READ')
READ(4,*) sim1
READ(4,*) sim2
READ(4,*) lambda
READ(4,*) a, b, c
READ(4,*) element
WRITE(*,*) sim1, sim2, lambda, a, b, c, element

IF (sim2 == 1) THEN
    CALL SIMP(sim1, sim2, lambda, a, element)
ELSE IF (sim2 == 2) THEN
    CALL FACE_CEN(sim1, sim2, lambda, a, element)
ELSE IF (sim2 == 3) THEN
    CALL BODY_CEN(sim1, sim2, lambda, a, element)
END IF

END PROGRAM TEST_1
