PROGRAM Lore

IMPLICIT NONE

INTEGER :: i
REAL :: f=850292.312, x0=43, pi

pi=4.*atan(1.)

Do i = 1,150
  f = 10/(pi*(((i-x0)**2)+10*2))
  If (i == 43) then
    f=850292.312
  Else if (i == 51) then
    f=466910.594
  Else if (i == 74) then
    f=199280.719
  Else if (i == 90) then
    f=377678.688
  Else if (i == 95) then
    f=63307.0898
  Else if (i == 117) then
    f=59775.5859
  Else if (i == 137) then
    f=66075.8750
  End If
  WRITE(*,*) "   dos theta      intensidad     ", i, f
End Do

END PROGRAM Lore