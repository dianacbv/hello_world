# Area de un cìrculo de radio r
program circulo 
  REAL :: r, area
  WRITE (*,*) "Escribe el radio r:"
  READ (*,*) r
  area = 3.1416*r*r
  WRITE (*,*) "Area = ", area
end program 
