PROGRAM SIM_TRANSF_1

!This program performs the Similariry Transformation of the Simetry Operations.

USE mod_matrix
    
    INTEGER :: n, L
    INTEGER :: x, y, z
    
    
    WRITE(*,*) 'Elija la Operaciòn de Simetrìa que desea verificar:' 
    WRITE(*,*) 'IDENTIDAD (E)   1'
    WRITE(*,*) 'INVERSION (i)   2'
    WRITE(*,*) 'ESPEJO EN x (s(yz))   3'
    WRITE(*,*) 'GIRO DE ORDEN 2 EN z (C2(xy))   4'
    READ(*,*) n
   
    IF (n == 1) THEN
        OPEN(UNIT=1, FILE='identidad.dat')
        READ(1,*) L
        WRITE(*,*) 'La dimensiòn de la matrìz es:', L
            CALL MATRIX_DIM(L,L)
        READ(1,*) A
        WRITE(*,*) 'La matrìz Identidad es:', A
        CLOSE(UNIT=1)
    ELSE IF (n == 2) THEN
        OPEN(UNIT=4, FILE='inversion.dat')
        READ(4,*) L
        WRITE(*,*) 'La dimensiòn de la matrìz es:', L
            CALL MATRIX_DIM(L,L)
        READ(4,*) A
        WRITE(*,*) 'La matrìz Inversiòn es:', A
        CLOSE(UNIT=4)
    ELSE IF (n == 3) THEN
        OPEN(UNIT=5, FILE='espejo_x.dat')
        READ(5,*) L
        WRITE(*,*) 'La dimensiòn de la matrìz es:', L
            CALL MATRIX_DIM(L,L)
        READ(5,*) A
        WRITE(*,*) 'La matrìz espejo en x es:', A
        CLOSE(UNIT=5)
    ELSE IF (n == 4) THEN
        OPEN(UNIT=8, FILE='giro_2_z.dat')
        READ(8,*) L
        WRITE(*,*) 'La dimensiòn de la matrìz es:', L
            CALL MATRIX_DIM(L,L)
        READ(8,*) A
        WRITE(*,*) 'La matrìz giro de orden 2 en z es:', A
        CLOSE(UNIT=8)
    END IF

    WRITE(*,*) '¿Cuàles es la coordenada x del punto que desea transformar?'
    READ(*,*) x
    WRITE(*,*) '¿Cuàles es la coordenada y del punto que desea transformar?'
    READ(*,*) y
    WRITE(*,*) '¿Cuàles es la coordenada z del punto que desea transformar?'
    READ(*,*) z
    
    b = (/x, y, z/)
    
    WRITE(*,*) 'El vector b es:', b
    
    CLOSE (UNIT=3)
    
    WRITE(*,*) 'El vector resultante es:', MAT_MULTIP(A,b,L) 
    
END PROGRAM SIM_TRANSF_1
