PROGRAM Fibonacci
        
    USE mod_fibonacci
    
    IMPLICIT NONE

    INTEGER :: Fnum, Fibo
    INTEGER :: F0, F1, F2  
        
    WRITE(*,*) 'How many Fibonacci numbers would you like?'
    READ(*,*) Fnum
    
    IF (Fnum == 0) THEN
        CALL F_n_0(0,F0)
    ELSE IF (Fnum == 1) THEN
        CALL F_n_0(0,F0)
        CALL F_n_1(1,F1)
    ELSE IF (Fnum == 2) THEN
        CALL F_n_0(0,F0)
        CALL F_n_1(1,F1)
        CALL F_n_1(2,F2)
    ELSE IF (Fnum >= 3) THEN
        
        CALL F_n_0(0,F0)
        CALL F_n_1(1,F1)
        CALL F_n_1(2,F2)     
        CALL Fib(Fnum,Fibo)
               
    END IF

    WRITE(*,*) 'THANKS FOR USING THE FIBONACCI SEQUENCE GENERATOR'

END PROGRAM Fibonacci
