! Resolucion de ecuaciones cuadraticas
PROGRAM cuadratica
REAL :: a, b, c, D, x1, x2
WRITE (*,*) "Cuales son los valores de a, b y c?"
READ (*,*) a, b, c
D = b**2 - 4*a*c
IF (D>0.0) THEN
x1 = (-b + D**1/2)/(2*a)
x2 = (-b - D**1/2)/(2*a)
WRITE (*,*) "Las soluciones son x1:", x1, "x2:", x2
ELSE IF (D == 0) THEN
x1 = -(b/2*a)
x2 = -(b/2*a)
WRITE (*,*) "Las soluciones son x1:", x1, "x2:", x2
ELSE
WRITE (*,*) "Las soluciones son x1 y x2: i"
END IF
END PROGRAM cuadratica
