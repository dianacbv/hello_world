PROGRAM LEER
IMPLICIT NONE
    INTEGER :: i
    real    :: x_tot = 0, y_tot = 0
    REAL    :: x(1:21), y(1:21)

    OPEN (unit=2, file="di_poli4.dat")
    write(*,*) "Reading file ..."
    DO i = 1,21
       READ (2, FMT=*) x(i), y(i)
    END DO
    CLOSE (unit=2)
    write(*,*) "... Reading file done"
    write(*,*) "    x(i)           y(i)"
    DO i = 1,21
       WRITE (*,*) x(i), y(i)
    END DO

    do i = 1, 21
        x_tot = x_tot + x(i)
        y_tot = y_tot + y(i)
    end do

    write(*,*) "x(tot) =", x_tot
    write(*,*) "y(tot) =", y_tot

END PROGRAM LEER