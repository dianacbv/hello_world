PROGRAM BIN
    IMPLICIT NONE

    INTEGER :: i, j
    REAL, DIMENSION(345,345) :: Inten
    REAL, DIMENSION(345,345) :: Test
    INTEGER :: IOstatus
    INTEGER :: log_uni
    INTEGER :: x
    CHARACTER(len = 256) :: fname

    open(unit= 10, status='old',file='2d_pat_1.raw',form='unformatted', access='stream', action='read')  ! open an existing file
    WRITE (*,*) "Opening file"

    Do i=1, 345
        Do j=1, 345
            read(10, IOSTAT=IOstatus) Inten ! read the data into array Inten, of the appropriate data type
        End Do
    End Do
    WRITE (*,*) "IOstatus=", IOstatus

    close(10) ! close the file

    Do i=1, 150
        Do j=1, 150
            Inten(i,j)=-1
        End Do
    End Do

    Do i=50, 100
        Do j=50, 100
            Inten(i,j)=9000000
        End Do
    End Do

    Do x=1,50
        Do i=2, 344
            Do j=2, 344
                Test(i,j)=(Inten(i,j-1)+Inten(i-1,j)+Inten(i,j+1)+Inten(i+1,j))/4
            End Do
        End Do
        Do i=2, 344
            Do j=2, 344
                Inten(i,j)=Test(i,j)
            End Do
        End Do
    End Do

    log_uni = 4
    fname = 'Test.raw'

    OPEN(unit = log_uni, file = fname, status = "replace", access = "stream", form = "unformatted")
        write(unit = log_uni) Inten
    close(unit = log_uni)

    write(unit=*,fmt = "(a)") " Result image in File: "//trim(fname)

END PROGRAM BIN
